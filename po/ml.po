# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Red Hat, Inc.
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Ani Peter <apeter@redhat.com>, 2012.
# mitr <mitr@volny.cz>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: volume_key 0.3.9\n"
"Report-Msgid-Bugs-To: https://fedorahosted.org/volume_key/\n"
"POT-Creation-Date: 2012-09-22 21:31+0200\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2015-03-14 08:39-0400\n"
"Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>\n"
"Language-Team: Malayalam <discuss@lists.smc.org.in>\n"
"Language: ml\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Zanata 4.6.2\n"

#: lib/crypto.c:65 lib/crypto.c:592
#, c-format
msgid "%s: %s"
msgstr "%s: %s"

#: lib/kmip.c:256 lib/kmip.c:372
msgid "Not enough space provided to store a KMIP packet"
msgstr "ഒരു KMIP പാക്കറ്റ് സൂക്ഷിക്കുന്നതിനാവശ്യമായ സ്ഥലം ലഭ്യമല്ല."

#: lib/kmip.c:323 lib/kmip.c:870
msgid "A string is too long"
msgstr "സ്ട്രിങ് വളരെ വലുതാണു്"

#: lib/kmip.c:341 lib/kmip.c:914
msgid "Binary data is too long"
msgstr "ബൈനറി ഡേറ്റാ വളരെ വലുതാണു്"

#: lib/kmip.c:392
msgid "A KMIP structure is too long"
msgstr "ഒരു KMIP സ്ട്രക്ചര്‍ വളരെ വലുതാണു്"

#: lib/kmip.c:739 lib/kmip.c:857 lib/kmip.c:920
msgid "Unexpected end of data"
msgstr "ഡേറ്റായുടെ അപ്രതീക്ഷിതമായ അവസാനം"

#: lib/kmip.c:764 lib/kmip.c:843 lib/kmip.c:900 lib/kmip.c:947
#, c-format
msgid "Unexpected item tag 0x%08lX"
msgstr "അപ്രതീക്ഷിതമായ ഐറ്റം ടാഗ് 0x%08lX"

#: lib/kmip.c:771 lib/kmip.c:907 lib/kmip.c:954
#, c-format
msgid "Unexpected item type 0x%02X"
msgstr "അപ്രതീക്ഷിതമായ ഐറ്റം തരം 0x%02X"

#: lib/kmip.c:777
msgid "Unexpected item size"
msgstr "അപ്രതീക്ഷിതമായ ഐറ്റം വ്യാപ്തി"

#: lib/kmip.c:818 lib/kmip.c:1275
#, c-format
msgid "Unsupported enum value %s"
msgstr "പിന്തുണയില്ലാത്ത enum മൂല്ല്യം %s"

#: lib/kmip.c:850
#, c-format
msgid "String item expected, got %02X"
msgstr "പ്രതീക്ഷിച്ചതു് സ്ട്രിങ് ഐറ്റം, ലഭ്യമായതു് %02X"

#: lib/kmip.c:962
msgid "Structure does not fit in its container"
msgstr "സ്ട്രക്ചര്‍ കണ്ടെയിനറില്‍ പാകമല്ല"

#: lib/kmip.c:978
msgid "Unexpected data in structure"
msgstr "സ്ട്രക്ചറില്‍ അപ്രതീക്ഷിതമായ ഡേറ്റാ"

#: lib/kmip.c:1064 lib/kmip.c:1297
msgid "Number of key bits is not positive"
msgstr "കീ ബിറ്റുകളുടെ എണ്ണം പോസിറ്റീവല്ല"

#: lib/kmip.c:1312
msgid "Wrapped key is not opaque"
msgstr "റാപ്പ്ഡ് കീ ഒപ്പേക് അല്ല"

#: lib/kmip.c:1350 lib/kmip.c:1386
#, c-format
msgid "Unsupported symmetric key format %s"
msgstr "പിന്തുണയില്ലാത്ത സിമ്മട്രിക്ക് കീ ശൈലി %s"

#: lib/kmip.c:1423 lib/volume.c:691
#, c-format
msgid "Unsupported KMIP version %s.%s"
msgstr "പിന്തുണയില്ലാത്ത KMIP പതിപ്പു് %s.%s"

#: lib/kmip.c:1473
#, c-format
msgid "Unsupported object type %s"
msgstr "പിന്തുണയില്ലാത്ത വസ്തു രീതി %s"

#: lib/kmip.c:1536
msgid "Unexpected data after packet"
msgstr "പാക്കറ്റിനു് ശേഷം അപ്രതീക്ഷിതമായ ഡേറ്റാ"

#: lib/kmip.c:1647 lib/kmip.c:1737 lib/kmip.c:1844 lib/kmip.c:1924
#: lib/volume.c:710 lib/volume_luks.c:805
#, c-format
msgid "Unsupported packet type %s"
msgstr "പിന്തുണയില്ലാത്ത പാക്കറ്റ് രീതി %s"

#: lib/kmip.c:1665 lib/kmip.c:1863
#, c-format
msgid "Unsupported mechanism %lu"
msgstr "പിന്തുണയില്ലാത്ത സംവിധാനം %lu"

#: lib/kmip.c:1752 lib/kmip.c:1775 lib/kmip.c:1937
msgid "Unexpected wrapped key format"
msgstr "അപ്രതീക്ഷിതമായ റാപ്പ്ഡ് കീ ആകൃതി"

#: lib/kmip.c:1765 lib/kmip.c:1950
msgid "Unsupported wrapping mechanism"
msgstr "പിന്തുണലഭ്യമല്ലാത്ത റാപ്പിങ് സംവിധാനം"

#: lib/libvolume_key.c:204
msgid "Unsupported asymmetric encryption format"
msgstr "പിന്തുണയില്ലാത്ത അസിമ്മെട്രിക് എന്‍ക്രിപ്ഷന്‍ മാതൃക "

#: lib/libvolume_key.c:323
msgid "Input packet is too small"
msgstr "ഇന്‍പുട്ട് പാക്കറ്റ് വളരെ ചെറുതാണു്"

#: lib/libvolume_key.c:333
msgid "Input is not a volume_key escrow packet"
msgstr "ഇന്‍പുട്ട് ഒരു volume_key എസ്ക്രോ പാക്കറ്റല്ല"

#: lib/libvolume_key.c:339
msgid "Unsupported packet format"
msgstr "പിന്തുണയില്ലാത്ത പാക്കറ്റ് മാതൃക"

#: lib/libvolume_key.c:406
msgid "Escrow packet passphrase"
msgstr "എസ്ക്രോ പാക്കറ്റ് പാസ്ഫ്രെയിസ്"

#: lib/libvolume_key.c:418 lib/volume_luks.c:312
msgid "Too many attempts to get a valid passphrase"
msgstr "ശരിയായ ഒരു പാസ്ഫ്രെയിസിനായി അനവധി ശ്രമങ്ങള്‍ നടത്തിയിരിക്കുന്നു"

#: lib/libvolume_key.c:462
msgid "Too many attempts to get a valid symmetric key"
msgstr "ശരിയായ ഒരു സിമ്മെട്രിക് കീ ലഭ്യമാകുന്നതിനുള്ള അനവധി ശ്രമങ്ങള്‍ "

#: lib/libvolume_key.c:514
msgid "The packet metadata is encrypted"
msgstr "പാക്കറ്റ് മെറ്റാഡേറ്റാ എന്‍ക്രിപ്റ്റ് ചെയ്തിരിയ്ക്കുന്നു"

#: lib/ui.c:148
msgid "User interface callbacks not provided"
msgstr "യൂസര്‍ ഇന്റര്‍ഫെയിസ് കോള്‍ബാക്കുകള്‍ ലഭ്യമല്ല"

#: lib/ui.c:154 src/volume_key.c:700
msgid "Passphrase not provided"
msgstr "പാസ്ഫ്രെയിസ് ലഭ്യമല്ല"

#: lib/ui.c:169
msgid "Symmetric key callback not provided"
msgstr "സിമ്മെട്രിക് കീ കോള്‍ബാക്ക് ലഭ്യമല്ല"

#: lib/ui.c:176
msgid "Symmetric key not provided"
msgstr "സിമ്മെട്രിക് കീ ലഭ്യമല്ല"

#: lib/volume.c:93
msgid "The key is too long"
msgstr "കീ വളരെ വലുതാണു്"

#: lib/volume.c:172 lib/volume.c:193
#, c-format
msgid "Required attribute `%s' not found"
msgstr "ആവശ്യമുള്ള വിശേഷത `%s' ലഭ്യമായില്ല"

#: lib/volume.c:315
#, c-format
msgid "Cannot get attributes of `%s'"
msgstr "`%s'-നുള്ള വിശേഷതകള്‍ ലഭ്യമാകുവാന്‍ സാധ്യമല്ല"

#: lib/volume.c:422
msgid "Host name"
msgstr "ഹോസ്റ്റ് നാമം"

#: lib/volume.c:424
msgid "Volume format"
msgstr "വോള്യം മാതൃക"

#: lib/volume.c:427
msgid "Volume UUID"
msgstr "വോള്യം UUID"

#: lib/volume.c:430
msgid "Volume label"
msgstr "വോള്യം ലേബല്‍"

#: lib/volume.c:432
msgid "Volume path"
msgstr "വോള്യം പാഥ്"

#: lib/volume.c:458 lib/volume.c:567 lib/volume.c:601 lib/volume.c:630
#: lib/volume.c:663 lib/volume.c:769
#, c-format
msgid "Volume `%s' has unsupported format"
msgstr "വോള്യം `%s'-ല്‍ പിന്തുണയില്ലാത്ത മാതൃക"

#: lib/volume.c:488
#, c-format
msgid "Volume format mismatch (packet `%s', volume `%s')"
msgstr "വോള്യം മാതൃക ചേരുന്നില്ല (പാക്കറ്റ് `%s', വോള്യം `%s')"

#: lib/volume.c:516
#, c-format
msgid "UUID mismatch (packet `%s', volume `%s')"
msgstr "UUID ചേരുന്നില്ല (പാക്കറ്റ് `%s', വോള്യം `%s')"

#: lib/volume.c:522
#, c-format
msgid "Host name mismatch (packet `%s', volume `%s')"
msgstr "ഹോസ്റ്റ് നാമം ചേരുന്നില്ല (പാക്കറ്റ് `%s', വോള്യം `%s')"

#: lib/volume.c:529
#, c-format
msgid "Volume label mismatch (packet `%s', volume `%s')"
msgstr "വോള്യം ലേബല്‍ ചേരുന്നില്ല (പാക്കറ്റ് `%s', വോള്യം `%s')"

#: lib/volume.c:536
#, c-format
msgid "Volume path mismatch (packet `%s', volume `%s')"
msgstr "വോള്യം പാഥ് ചേരുന്നില്ല (പാക്കറ്റ് `%s', വോള്യം `%s')"

#: lib/volume.c:746
#, c-format
msgid "Unsupported volume format `%s'"
msgstr "പിന്തുണയില്ലാത്ത വോള്യം മാതൃക `%s'"

#: lib/volume_luks.c:55
#, c-format
msgid "Unknown error %d"
msgstr "അപരിചിതമായ പിശക് %d"

#: lib/volume_luks.c:105
#, c-format
msgid "Error getting information about volume `%s': "
msgstr "വോള്യം `%s'-നെപ്പറ്റിയുള്ള വിവരം ലഭ്യമാകുന്നതില്‍ പിശക്: "

#: lib/volume_luks.c:187
#, c-format
msgid "UUID mismatch between libblkid and libcryptsetup: `%s' vs. `%s'"
msgstr "libblkid, libcryptsetup എന്നിവയുടെ UUID ചേരുന്നില്ല: `%s' vs. `%s'"

#: lib/volume_luks.c:212
msgid "LUKS cipher name"
msgstr "LUKS സിഫര്‍ നാമം"

#: lib/volume_luks.c:215
msgid "LUKS cipher mode"
msgstr "LUKS സിഫര്‍ മോഡ്"

#: lib/volume_luks.c:218
msgid "Key size (bits)"
msgstr "കീയുടെ വ്യാപ്തി (ബിറ്റുകള്‍)"

#: lib/volume_luks.c:238
msgid "Data encryption key"
msgstr "ഡേറ്റാ എന്‍ക്രിപ്ഷന്‍ കീ"

#: lib/volume_luks.c:242
msgid "Passphrase"
msgstr "പാസ്ഫ്രെയിസ്"

#: lib/volume_luks.c:245
msgid "Passphrase slot"
msgstr "പാസ്ഫ്രെയിസ് സ്ലോട്ട്"

#: lib/volume_luks.c:276 lib/volume_luks.c:457 lib/volume_luks.c:698
msgid "Encryption information type unsupported in LUKS"
msgstr "LUKS-ല്‍ പിന്തുണയില്ലാത്ത തരത്തിലുള്ള എന്‍ക്രിപ്ഷന്‍ വിവരം"

#: lib/volume_luks.c:284
#, c-format
msgid "Passphrase for `%s'"
msgstr "`%s'-നുള്ള പാസ്ഫ്രെയിസ്"

#: lib/volume_luks.c:307 lib/volume_luks.c:850
msgid "Error getting LUKS data encryption key: "
msgstr "LUKS ഡേറ്റാ എന്‍ക്രിപ്ഷന്‍ കീ ലഭ്യമാക്കുന്നതില്‍ പിശക്: "

#: lib/volume_luks.c:354
#, c-format
msgid "Cipher name mismatch (packet `%s', volume `%s')"
msgstr "സിഫര്‍ നാമം ചേരുന്നില്ല (പാക്കറ്റ് `%s', വോള്യം `%s')"

#: lib/volume_luks.c:361
#, c-format
msgid "Cipher mode mismatch (packet `%s', volume `%s')"
msgstr "സിഫര്‍ മോഡ് ചേരുന്നില്ല (പാക്കറ്റ് `%s', വോള്യം `%s')"

#: lib/volume_luks.c:368
#, c-format
msgid "Key size mismatch (packet %zu, volume %zu)"
msgstr "കീയുടെ വ്യാപ്തി ചേരുന്നില്ല (പാക്കറ്റ് %zu, വോള്യം %zu)"

#: lib/volume_luks.c:399
msgid "LUKS data encryption key in packet is invalid: "
msgstr "പാക്കറ്റിലുള്ള LUKS ഡേറ്റാ എന്‍ക്രിപ്ഷന്‍ കീ അസാധുവാണു്: "

#: lib/volume_luks.c:424
msgid "LUKS passphrase in packet is invalid: "
msgstr "പാക്കറ്റിലുള്ള LUKS പാസ്ഫ്രെയിസ് അസാധുവാണു്: "

#: lib/volume_luks.c:463
msgid "Escrow packet does not contain the LUKS data encryption key"
msgstr "എസ്ക്രോ പാക്കറ്റില്‍ LUKS ഡേറ്റാ എന്‍ക്രിപ്ഷന്‍ കീ ലഭ്യമല്ല"

#: lib/volume_luks.c:468
#, c-format
msgid "New passphrase for `%s'"
msgstr "`%s'-നുള്ള പുതിയ പാസ്ഫ്രെയിസ്"

#: lib/volume_luks.c:469
#, c-format
msgid "Repeat new passphrase for `%s'"
msgstr "`%s'-നുള്ള പുതിയ പാസ്ഫ്രെയിസ് ആവര്‍ത്തിക്കുക"

#: lib/volume_luks.c:470
#, c-format
msgid "Passphrases do not match.  New passphrase for `%s'"
msgstr "പാസ്ഫ്രെയിസുകള്‍ തമ്മില്‍ ചേരുന്നില്ല.  `%s'-നുള്ള പുതിയ പാസ്ഫ്രെയിസ്"

#: lib/volume_luks.c:497 src/volume_key.c:690
msgid "Too many attempts to get a passphrase"
msgstr "ഒരു പാസ്ഫ്രെയിസിനായി അനവധി ശ്രമങ്ങള്‍ നടത്തിയിരിക്കുന്നു"

#: lib/volume_luks.c:512 lib/volume_luks.c:574
msgid "Error adding a LUKS passphrase"
msgstr "ഒരു LUKS പാസ്ഫ്രെയിസ് ചേര്‍ക്കുന്നതില്‍ പിശക്"

#: lib/volume_luks.c:550
msgid "Can not add a secret of this type"
msgstr "ഈതരത്തിലുള്ള ഒരു രഹസ്യം ചേര്‍ക്കുവാന്‍ സാധ്യമല്ല"

#: lib/volume_luks.c:556 lib/volume_luks.c:656 lib/volume_luks.c:858
msgid "Data encryption key unknown"
msgstr "ഡേറ്റാ എന്‍ക്രിപ്ഷന്‍ കീ അപരിചിതം"

#: lib/volume_luks.c:562
msgid "The passphrase must be a string"
msgstr "പാസ്ഫ്രെയിസ് ഒരു സ്ട്രിങായിരിക്കണം"

#: lib/volume_luks.c:679
msgid "Passphrase unknown"
msgstr "പാസ്ഫ്രെയിസ് അപരിചിതം"

#: lib/volume_luks.c:742
#, c-format
msgid "Unsupported key length %s"
msgstr "പിന്തുണയില്ലാത്ത കീ വ്യാപ്തി %s"

#: lib/volume_luks.c:754
msgid "Key length mismatch"
msgstr "കീയുടെ നീളം ചേരുന്നില്ല"

#: lib/volume_luks.c:778
#, c-format
msgid "Invalid slot number `%s'"
msgstr "അസാധുവായ സ്ലോട്ട് നംബര്‍ `%s'"

#: lib/volume_luks.c:789
msgid "NUL byte in passphrase"
msgstr "പാസ്ഫ്രെയിസില്‍ NUL ബൈറ്റ്"

#: lib/volume_luks.c:866
msgid "Error opening LUKS volume: "
msgstr "LUKS വോള്യം തുറക്കുന്നതില്‍ പിശക്: "

#: python/volume_key.i:369
msgid "Error decoding certificate"
msgstr "സര്‍ട്ടിഫിക്കേറ്റ് ഡീകോഡ് ചെയ്യുന്നതില്‍ പിശക്"

#: src/volume_key.c:53 src/volume_key.c:495 src/volume_key.c:550
#, c-format
msgid "%s: "
msgstr "%s: "

#. TRANSLATORS: The "(y/n)" part should indicate to the user that input
#. matching (locale yesexpr) and (locale noexpr) is expected.
#: src/volume_key.c:83
#, c-format
msgid "%s (y/n) "
msgstr "%s (y/n) "

#: src/volume_key.c:176
msgid "Show version"
msgstr "പതിപ്പു് കാണിക്കുക"

#: src/volume_key.c:181
msgid "Save volume secrets to a packet.  Expects operands VOLUME [PACKET]."
msgstr ""
"ഒരു പാക്കറ്റിലേക്ക് വോള്യം രഹസ്യങ്ങള്‍ സൂക്ഷിക്കുക.  ഓപറണ്ടസ് VOLUME "
"[PACKET] പ്രതീക്ഷിക്കുന്നു."

#: src/volume_key.c:186
msgid "Restore volume secrets from a packet.  Expects operands VOLUME PACKET."
msgstr ""
"ഒരു പാക്കറ്റില്‍ നിന്നുമുള്ള വോള്യം രഹസ്യങ്ങള്‍ വീണ്ടെടുക്കുക.  ഓപറണ്ടസ് "
"VOLUME PACKET പ്രതീക്ഷിക്കുന്നു."

#: src/volume_key.c:191
msgid ""
"Set up an encrypted volume using secrets from a packet.  Expects operands "
"VOLUME PACKET NAME."
msgstr ""
"ഒരു പാക്കറ്റില്‍ നിന്നുള്ള രഹസ്യങ്ങളുപയോഗിച്ചു് ഒരു എന്‍ക്രിപ്റ്റഡ് വോള്യം "
"സജ്ജമാക്കുക. ഓപറണ്ടസ് VOLUME PACKET NAME പ്രതീക്ഷിക്കുന്നു."

#: src/volume_key.c:196
msgid "Re-encrypt an escrow packet.  Expects operand PACKET."
msgstr ""
"ഒരു എസ്ക്രോ പാക്കറ്റ് വീണ്ടും എന്‍ക്രിപ്റ്റ് ചെയ്യുക.  ഓപറണ്ട് PACKET "
"പ്രതീക്ഷിക്കുന്നു."

#: src/volume_key.c:200
msgid "Show information contained in a packet.  Expects operand PACKET."
msgstr ""
"ഒരു പാക്കറ്റിലുള്ള വിവരങ്ങള്‍ കാണിക്കുക.  ഓപറണ്ട് PACKET പ്രതീക്ഷിക്കുന്നു."

#: src/volume_key.c:205
msgid "Show secrets contained in a packet.  Expects operand PACKET."
msgstr ""
"ഒരു പാക്കറ്റിലുള്ള രഹസ്യങ്ങള്‍ കാണിക്കുക.  ഓപറണ്ട് PACKET പ്രതീക്ഷിക്കുന്നു."

#: src/volume_key.c:210
msgid "Use the NSS database in DIR"
msgstr "DIR-ലുള്ള NSS ഡേറ്റാബെയിസ് ഉപയോഗിക്കുക"

#: src/volume_key.c:210
msgid "DIR"
msgstr "DIR"

#: src/volume_key.c:213
msgid "Run in batch mode"
msgstr "ബാച്ച് മോഡില്‍ പ്രവര്‍ത്തിപ്പിക്കുക"

#: src/volume_key.c:219
msgid "Write the default secret to PACKET"
msgstr "സഹജമായ രഹസ്യം പാക്കറ്റിലേക്ക് സൂക്ഷിക്കുക"

#: src/volume_key.c:219 src/volume_key.c:224 src/volume_key.c:228
#: src/volume_key.c:233
msgid "PACKET"
msgstr "PACKET"

#: src/volume_key.c:223
msgid "Write data encryption key to PACKET"
msgstr "പാക്കറ്റിലേക്ക് ഡേറ്റാ എന്‍ക്രിപ്ഷന്‍ കീ സൂക്ഷിക്കുക"

#: src/volume_key.c:228
msgid "Write passphrase to PACKET"
msgstr "പാക്കറ്റിലേക്ക് പാസ്ഫ്രെയിസ് സൂക്ഷിക്കുക"

#: src/volume_key.c:233
msgid "Create a random passphrase and write it to PACKET"
msgstr "പെട്ടെന്നൊരു പാസ്ഫ്രെയിസ് ഉണ്ടാക്കി അതു് പാക്കറ്റിലേക്ക് സൂക്ഷിക്കുക"

#: src/volume_key.c:241
msgid "Encrypt for the certificate in CERT"
msgstr "സര്‍ട്ടിഫിക്കേറ്റിനുള്ള എന്‍ക്രിപ്റ്റ് CERT-ല്‍"

#: src/volume_key.c:241
msgid "CERT"
msgstr "CERT"

#: src/volume_key.c:245
msgid "Use FORMAT for all output packets"
msgstr "എല്ലാ ഔട്ട്പുട്ട് പാക്കറ്റുകള്‍ക്കും FORMAT ഉപയോഗിക്കുക"

#: src/volume_key.c:245
msgid "FORMAT"
msgstr "FORMAT"

#: src/volume_key.c:249
msgid "Only include unencrypted information, if any, in --dump"
msgstr ""
"എന്‍ക്രിപ്റ്റ് ചെയ്യാത്ത വിവരം മാത്രം ഉള്‍പ്പെടുത്തുക, ഉണ്ടെങ്കില്‍, --dump-"
"ല്‍"

#: src/volume_key.c:253
msgid "Include secrets in --dump output"
msgstr "--dump ഔട്ട്പുട്ടില്‍ രഹസ്യങ്ങള്‍ ഉള്‍പ്പെടുത്തുക"

#: src/volume_key.c:268
msgid "OPERANDS"
msgstr "OPERANDS"

#: src/volume_key.c:270
msgid "Manages encrypted volume keys and passphrases."
msgstr ""
"എന്‍ക്രിപ്റ്റ് ചെയ്ത വോള്യം കീകളും പാസ്ഫ്രെയിസുകളും കൈകാര്യം ചെയ്യുന്നു."

#: src/volume_key.c:271
#, c-format
msgid "Report bugs to %s"
msgstr "%s-ല്‍ ബഗുകള്‍ രേഖപ്പെടുത്തുന്നു"

#: src/volume_key.c:279
#, c-format
msgid "%s: %s\n"
"Run `%s --help' for more information.\n"
msgstr "%s: %s\n"
"കൂടുതല്‍ വിവരങ്ങള്‍ക്കായി `%s --help' പ്രവര്‍ത്തിപ്പിക്കുക.\n"

#: src/volume_key.c:290
msgid ""
"Copyright (C) 2009 Red Hat, Inc. All rights reserved.\n"
"This software is distributed under the GPL v.2.\n"
"\n"
"This program is provided with NO WARRANTY, to the extent permitted by law."
msgstr ""
"Copyright (C) 2009 Red Hat, Inc. All rights reserved.\n"
"This software is distributed under the GPL v.2.\n"
"\n"
"This program is provided with NO WARRANTY, to the extent permitted by law."

#: src/volume_key.c:302
msgid "Operation mode not specified"
msgstr "ഓപ്പറേഷന്‍ മോഡ് വ്യക്തമാക്കിയിട്ടില്ല"

#: src/volume_key.c:308
msgid "Ambiguous operation mode"
msgstr "വ്യക്തമല്ലാത്ത ഓപ്പറേഷന്‍ മോഡ്"

#: src/volume_key.c:312 src/volume_key.c:314 src/volume_key.c:367
#, c-format
msgid "`--%s' is only valid with `--%s'"
msgstr "`--%s', `--%s'-നൊപ്പം മാത്രമേ ശരിയാകൂ"

#: src/volume_key.c:320
msgid "Output can be specified only with `--save' or `--reencrypt'"
msgstr ""
"`--save' അല്ലെങ്കില്‍ `--reencrypt'ഉപയോഗിച്ചു് മാത്രമേ ഔട്ട്പുട്ട് "
"വ്യക്തമാക്കുവാന്‍ പാടുള്ളൂ"

#: src/volume_key.c:327
msgid "No output specified"
msgstr "ഔട്ട്പുട്ട് വ്യക്തമാക്കിയിട്ടില്ല"

#: src/volume_key.c:329
msgid "Ambiguous output format"
msgstr "വ്യക്തമല്ലാത്ത ഔട്ട്പുട്ട് മാതൃക"

#: src/volume_key.c:344
#, c-format
msgid "Unknown packet format `%s'"
msgstr "അപരിചിതമായ പാക്കറ്റ് മാതൃക `%s'"

#: src/volume_key.c:354
msgid "Output format does not match other options"
msgstr "ഔട്ട്പുട്ട് മാതൃകകള്‍ മറ്റു് ഉപാധികളുമായി പൊരുത്തപ്പെടുന്നില്ല"

#: src/volume_key.c:469 src/volume_key.c:549
#, c-format
msgid "Error, try again.\n"
msgstr "പിശക്, വീണ്ടും ശ്രമിക്കുക.\n"

#: src/volume_key.c:470
#, c-format
msgid "Enter password for `%s': "
msgstr "`%s'-നുള്ള അടയാളവാക്ക് നല്‍കുക: "

#: src/volume_key.c:584 src/volume_key.c:623
#, c-format
msgid "Error reading `%s': "
msgstr "`%s' ലഭ്യമാക്കുന്നതില്‍ പിശക്: "

#: src/volume_key.c:591 src/volume_key.c:631
#, c-format
msgid "Error decoding `%s': "
msgstr "`%s' ഡീകോഡ് ചെയ്യുന്നതില്‍ പിശക്: "

#: src/volume_key.c:666
msgid "New packet passphrase"
msgstr "പുതിയ പാക്കറ്റ് പാസ്ഫ്രെയിസ്"

#: src/volume_key.c:667
msgid "Passphrases do not match.  New packet passphrase"
msgstr "പാസ്ഫ്രെയിസുകള്‍ തമ്മില്‍ ചേരുന്നില്ല. പുതിയ പാക്കറ്റ് പാസ്ഫ്രെയിസ്"

#: src/volume_key.c:674
msgid "Repeat new packet passphrase"
msgstr "പുതിയ പാക്കറ്റ് പാസ്ഫ്രെയിസ് ആവര്‍ത്തിക്കുക"

#: src/volume_key.c:737
#, c-format
msgid "Error creating `%s': "
msgstr "`%s' ഉണ്ടാക്കുന്നതില്‍ പിശക്: "

#: src/volume_key.c:787
#, c-format
msgid "Error generating passphrase: %s"
msgstr "ഒരു പാസ്ഫ്രെയിസ് ഉണ്ടാക്കുന്നതില്‍ പിശക്: %s"

#: src/volume_key.c:821
#, c-format
msgid "Usage: %s --save VOLUME [PACKET]"
msgstr "ഉപയോഗിക്കേണ്ട വിധം: %s --save VOLUME [PACKET]"

#: src/volume_key.c:829 src/volume_key.c:844 src/volume_key.c:957
#: src/volume_key.c:995
#, c-format
msgid "Error opening `%s': %s"
msgstr "`%s' തുറക്കുന്നതില്‍ പിശക്: %s"

#: src/volume_key.c:840
#, c-format
msgid "Error loading `%s': %s"
msgstr "`%s' ലഭ്യമാക്കുന്നതില്‍ പിശക്: %s"

#: src/volume_key.c:858
#, c-format
msgid "Error creating a passphrase: %s"
msgstr "ഒരു പാസ്ഫ്രെയിസ് ഉണ്ടാക്കുന്നതില്‍ പിശക്: %s"

#: src/volume_key.c:890
#, c-format
msgid "`%s' does not match `%s': "
msgstr "`%s'`%s'-മായി പൊരുത്തപ്പെടുന്നില്ല: "

#: src/volume_key.c:899
#, c-format
msgid "`%s' perhaps does not match `%s'\n"
msgstr "`%s' ഒരു പക്ഷേ `%s'-മായി പൊരുത്തപ്പെടുന്നില്ല\n"

#: src/volume_key.c:914
msgid "Are you sure you want to use this packet?"
msgstr "നിങ്ങള്‍ക്ക് ഈ പാക്കറ്റ് ഉപയോഗിക്കണമെന്നുറപ്പാണോ?"

#: src/volume_key.c:926
msgid "Error getting a yes/no answer"
msgstr "yes/no മറുപടി കിട്ടുന്നതില്‍ പിശക്"

#: src/volume_key.c:952
#, c-format
msgid "Usage: %s --%s VOLUME PACKET"
msgstr "ഉപയോഗിക്കേണ്ട വിധം: %s --%s VOLUME PACKET"

#: src/volume_key.c:973
#, c-format
msgid "Error restoring access to `%s': %s"
msgstr "`%s'-ലേക്കുള്ള അനുമതി വീണ്ടും ലഭ്യമാക്കുന്നതില്‍ പിശക്: %s"

#: src/volume_key.c:989
#, c-format
msgid "Usage: %s --%s VOLUME PACKET NAME"
msgstr "ഉപയോഗിക്കേണ്ട വിധം: %s --%s VOLUME PACKET NAME"

#: src/volume_key.c:1011
#, c-format
msgid "Error setting up `%s': %s"
msgstr "'%s' സജ്ജീകരിക്കുന്നതില്‍ പിശക്: %s"

#: src/volume_key.c:1026 src/volume_key.c:1057
#, c-format
msgid "Usage: %s --%s PACKET"
msgstr "ഉപയോഗിക്കേണ്ട വിധം: %s --%s PACKET"

#: src/volume_key.c:1062
#, c-format
msgid "Error reading `%s': %s"
msgstr "`%s' ലഭ്യമാക്കുന്നതില്‍ പിശക്: %s"

#: src/volume_key.c:1067
#, c-format
msgid "Invalid packet: %s"
msgstr "അസാധുവായ പാക്കറ്റ്: %s"

#: src/volume_key.c:1070
msgid "Unencrypted"
msgstr "എന്‍ക്രിപ്റ്റ് ചെയ്തിട്ടില്ലാത്ത"

#: src/volume_key.c:1074
msgid "Public key-encrypted"
msgstr "പബ്ലിക് കീ-എന്‍ക്രിപ്റ്റഡ്"

#: src/volume_key.c:1078
msgid "Passphrase-encrypted"
msgstr "പാസ്ഫ്രെയിസ്-എന്‍ക്രിപ്റ്റഡ്"

#: src/volume_key.c:1082
msgid "Only secrets public key-encrypted"
msgstr "രഹസ്യ പബ്ലിക് കീ മാത്രം-എന്‍ക്രിപ്റ്റഡ്"

#: src/volume_key.c:1086
msgid "Only secrets symmetric key-encrypted"
msgstr "രഹസ്യ സിമ്മെട്രിക് കീ മാത്രം-എന്‍ക്രിപ്റ്റഡ്"

#: src/volume_key.c:1093 src/volume_key.c:1122
#, c-format
msgid "%s:\t%s\n"
msgstr "%s:\t%s\n"

#: src/volume_key.c:1093
msgid "Packet format"
msgstr "പാക്കറ്റ് മാതൃക"

#: src/volume_key.c:1107
#, c-format
msgid "Error decoding `%s': %s"
msgstr "`%s' ഡീകോഡ് ചെയ്യുന്നതില്‍ പിശക്: %s"
